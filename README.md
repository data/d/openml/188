# OpenML dataset: eucalyptus

https://www.openml.org/d/188

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Bruce Bulloch    
**Source**: [WEKA Dataset Collection](http://www.cs.waikato.ac.nz/ml/weka/datasets.html) - part of the agridatasets archive. [This is the true source](http://tunedit.org/repo/Data/Agricultural/eucalyptus.arff)  
**Please cite**: None  

**Eucalyptus Soil Conservation**  
The objective was to determine which seedlots in a species are best for soil conservation in seasonally dry hill country. Determination is found by measurement of height, diameter by height, survival, and other contributing factors. 
 
It is important to note that eucalypt trial methods changed over time; earlier trials included mostly 15 - 30cm tall seedling grown in peat plots and the later trials have included mostly three replications of eight trees grown. This change may contribute to less significant results.

Experimental data recording procedures which require noting include:
 - instances with no data recorded due to experimental recording procedures
   require that the absence of a species from one replicate at a site was
   treated as a missing value, but if absent from two or more replicates at a
   site the species was excluded from the site's analyses.
 - missing data for survival, vigour, insect resistance, stem form, crown form
   and utility especially for the data recorded at the Morea Station; this 
   could indicate the death of species in these areas or a lack in collection
   of data.  

### Attribute Information  
 
  1.  Abbrev - site abbreviation - enumerated
  2.  Rep - site rep - integer
  3.  Locality - site locality in the North Island - enumerated
  4.  Map_Ref - map location in the North Island - enumerated
  5.  Latitude - latitude approximation - enumerated
  6.  Altitude - altitude approximation - integer
  7.  Rainfall - rainfall (mm pa) - integer
  8.  Frosts - frosts (deg. c) - integer
  9.  Year - year of planting - integer
  10. Sp - species code - enumerated
  11. PMCno - seedlot number - integer
  12. DBH - best diameter base height (cm) - real
  13. Ht - height (m) - real
  14. Surv - survival - integer
  15. Vig - vigour - real
  16. Ins_res - insect resistance - real
  17. Stem_Fm - stem form - real
  18. Crown_Fm - crown form - real
  19. Brnch_Fm - branch form - real
  Class:
  20. Utility - utility rating - enumerated

### Relevant papers

Bulluch B. T., (1992) Eucalyptus Species Selection for Soil Conservation in Seasonally Dry Hill Country - Twelfth Year Assessment  New Zealand Journal of Forestry Science 21(1): 10 - 31 (1991)  

Kirsten Thomson and Robert J. McQueen (1996) Machine Learning Applied to Fourteen Agricultural Datasets. University of Waikato Research Report  
https://www.cs.waikato.ac.nz/ml/publications/1996/Thomson-McQueen-96.pdf + the original publication:

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/188) of an [OpenML dataset](https://www.openml.org/d/188). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/188/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/188/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/188/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

